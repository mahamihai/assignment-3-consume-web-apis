﻿namespace HM3.VIEW.Views
{
    partial class TeacherView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.dataGridView4 = new System.Windows.Forms.DataGridView();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.emailBox = new System.Windows.Forms.TextBox();
            this.button3 = new System.Windows.Forms.Button();
            this.email = new System.Windows.Forms.TextBox();
            this.name = new System.Windows.Forms.TextBox();
            this.grupa = new System.Windows.Forms.TextBox();
            this.hobby = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.labNr = new System.Windows.Forms.TextBox();
            this.curricula = new System.Windows.Forms.TextBox();
            this.description = new System.Windows.Forms.TextBox();
            this.labDate = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.button9 = new System.Windows.Forms.Button();
            this.button13 = new System.Windows.Forms.Button();
            this.button14 = new System.Windows.Forms.Button();
            this.button15 = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.assiName = new System.Windows.Forms.TextBox();
            this.assiDescription = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.labe20 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.assiDate = new System.Windows.Forms.DateTimePicker();
            this.dataGridView3 = new System.Windows.Forms.DataGridView();
            this.button12 = new System.Windows.Forms.Button();
            this.button11 = new System.Windows.Forms.Button();
            this.button10 = new System.Windows.Forms.Button();
            this.dataGridView5 = new System.Windows.Forms.DataGridView();
            this.button18 = new System.Windows.Forms.Button();
            this.button17 = new System.Windows.Forms.Button();
            this.subId = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView5)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(41, 40);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(563, 232);
            this.dataGridView1.TabIndex = 0;
            this.dataGridView1.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick2);
            // 
            // dataGridView2
            // 
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Location = new System.Drawing.Point(41, 349);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.Size = new System.Drawing.Size(563, 182);
            this.dataGridView2.TabIndex = 1;
            this.dataGridView2.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView2_CellContentClick2);
            // 
            // dataGridView4
            // 
            this.dataGridView4.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView4.Location = new System.Drawing.Point(41, 860);
            this.dataGridView4.Name = "dataGridView4";
            this.dataGridView4.Size = new System.Drawing.Size(563, 182);
            this.dataGridView4.TabIndex = 3;
            this.dataGridView4.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView4_CellContentClick2);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(657, 40);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(104, 22);
            this.button1.TabIndex = 4;
            this.button1.Text = "Refresh Students";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(657, 80);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(104, 24);
            this.button2.TabIndex = 5;
            this.button2.Text = "Create Student";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // emailBox
            // 
            this.emailBox.Location = new System.Drawing.Point(947, 83);
            this.emailBox.Name = "emailBox";
            this.emailBox.Size = new System.Drawing.Size(100, 20);
            this.emailBox.TabIndex = 6;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(657, 128);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(104, 25);
            this.button3.TabIndex = 7;
            this.button3.Text = "Delete Students";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // email
            // 
            this.email.Location = new System.Drawing.Point(947, 191);
            this.email.Name = "email";
            this.email.Size = new System.Drawing.Size(100, 20);
            this.email.TabIndex = 9;
            // 
            // name
            // 
            this.name.Location = new System.Drawing.Point(947, 217);
            this.name.Name = "name";
            this.name.Size = new System.Drawing.Size(100, 20);
            this.name.TabIndex = 10;
            // 
            // grupa
            // 
            this.grupa.Location = new System.Drawing.Point(947, 243);
            this.grupa.Name = "grupa";
            this.grupa.Size = new System.Drawing.Size(100, 20);
            this.grupa.TabIndex = 11;
            // 
            // hobby
            // 
            this.hobby.Location = new System.Drawing.Point(947, 269);
            this.hobby.Name = "hobby";
            this.hobby.Size = new System.Drawing.Size(100, 20);
            this.hobby.TabIndex = 12;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(890, 198);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(32, 13);
            this.label2.TabIndex = 14;
            this.label2.Text = "Email";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(890, 224);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(35, 13);
            this.label3.TabIndex = 15;
            this.label3.Text = "Name";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(890, 250);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(36, 13);
            this.label4.TabIndex = 16;
            this.label4.Text = "Grupa";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(890, 276);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(38, 13);
            this.label5.TabIndex = 17;
            this.label5.Text = "Hobby";
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(657, 207);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(104, 39);
            this.button4.TabIndex = 18;
            this.button4.Text = "Update student";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(657, 348);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(104, 37);
            this.button5.TabIndex = 19;
            this.button5.Text = "Refresh Laboratories";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(657, 391);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(104, 37);
            this.button6.TabIndex = 20;
            this.button6.Text = "Create Laboratory";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(657, 434);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(104, 37);
            this.button7.TabIndex = 21;
            this.button7.Text = "Delete Laboratory";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // button8
            // 
            this.button8.Location = new System.Drawing.Point(657, 477);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(104, 37);
            this.button8.TabIndex = 22;
            this.button8.Text = "Update Lab";
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // labNr
            // 
            this.labNr.Location = new System.Drawing.Point(947, 349);
            this.labNr.Name = "labNr";
            this.labNr.Size = new System.Drawing.Size(200, 20);
            this.labNr.TabIndex = 23;
            // 
            // curricula
            // 
            this.curricula.Location = new System.Drawing.Point(947, 401);
            this.curricula.Name = "curricula";
            this.curricula.Size = new System.Drawing.Size(200, 20);
            this.curricula.TabIndex = 25;
            // 
            // description
            // 
            this.description.Location = new System.Drawing.Point(947, 427);
            this.description.Name = "description";
            this.description.Size = new System.Drawing.Size(200, 20);
            this.description.TabIndex = 26;
            // 
            // labDate
            // 
            this.labDate.Location = new System.Drawing.Point(947, 375);
            this.labDate.Name = "labDate";
            this.labDate.Size = new System.Drawing.Size(200, 20);
            this.labDate.TabIndex = 29;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(873, 356);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(62, 13);
            this.label1.TabIndex = 30;
            this.label1.Text = "LabNumber";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(873, 382);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(30, 13);
            this.label6.TabIndex = 31;
            this.label6.Text = "Date";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(874, 408);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(48, 13);
            this.label7.TabIndex = 32;
            this.label7.Text = "Curricula";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(873, 434);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(60, 13);
            this.label8.TabIndex = 33;
            this.label8.Text = "Description";
            // 
            // button9
            // 
            this.button9.Location = new System.Drawing.Point(657, 991);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(104, 37);
            this.button9.TabIndex = 40;
            this.button9.Text = "Update Assignment";
            this.button9.UseVisualStyleBackColor = true;
            // 
            // button13
            // 
            this.button13.Location = new System.Drawing.Point(657, 948);
            this.button13.Name = "button13";
            this.button13.Size = new System.Drawing.Size(104, 37);
            this.button13.TabIndex = 39;
            this.button13.Text = "Delete Assignment";
            this.button13.UseVisualStyleBackColor = true;
            this.button13.Click += new System.EventHandler(this.button13_Click);
            // 
            // button14
            // 
            this.button14.Location = new System.Drawing.Point(657, 905);
            this.button14.Name = "button14";
            this.button14.Size = new System.Drawing.Size(104, 37);
            this.button14.TabIndex = 38;
            this.button14.Text = "Create Assignment";
            this.button14.UseVisualStyleBackColor = true;
            this.button14.Click += new System.EventHandler(this.button14_Click);
            // 
            // button15
            // 
            this.button15.Location = new System.Drawing.Point(657, 862);
            this.button15.Name = "button15";
            this.button15.Size = new System.Drawing.Size(104, 37);
            this.button15.TabIndex = 37;
            this.button15.Text = "Refresh Assignments";
            this.button15.UseVisualStyleBackColor = true;
            this.button15.Click += new System.EventHandler(this.button15_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(903, 90);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(32, 13);
            this.label9.TabIndex = 41;
            this.label9.Text = "Email";
            // 
            // assiName
            // 
            this.assiName.Location = new System.Drawing.Point(947, 879);
            this.assiName.Name = "assiName";
            this.assiName.Size = new System.Drawing.Size(100, 20);
            this.assiName.TabIndex = 42;
            // 
            // assiDescription
            // 
            this.assiDescription.Location = new System.Drawing.Point(947, 931);
            this.assiDescription.Multiline = true;
            this.assiDescription.Name = "assiDescription";
            this.assiDescription.Size = new System.Drawing.Size(178, 82);
            this.assiDescription.TabIndex = 44;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(851, 886);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(35, 13);
            this.label10.TabIndex = 46;
            this.label10.Text = "Name";
            // 
            // labe20
            // 
            this.labe20.AutoSize = true;
            this.labe20.Location = new System.Drawing.Point(851, 938);
            this.labe20.Name = "labe20";
            this.labe20.Size = new System.Drawing.Size(60, 13);
            this.labe20.TabIndex = 47;
            this.labe20.Text = "Description";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(851, 912);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(49, 13);
            this.label12.TabIndex = 48;
            this.label12.Text = "Deadline";
            // 
            // assiDate
            // 
            this.assiDate.Location = new System.Drawing.Point(947, 905);
            this.assiDate.Name = "assiDate";
            this.assiDate.Size = new System.Drawing.Size(200, 20);
            this.assiDate.TabIndex = 50;
            // 
            // dataGridView3
            // 
            this.dataGridView3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView3.Location = new System.Drawing.Point(41, 603);
            this.dataGridView3.Name = "dataGridView3";
            this.dataGridView3.Size = new System.Drawing.Size(291, 182);
            this.dataGridView3.TabIndex = 2;
            this.dataGridView3.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView3_CellContentClick2);
            // 
            // button12
            // 
            this.button12.Location = new System.Drawing.Point(358, 602);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(104, 37);
            this.button12.TabIndex = 34;
            this.button12.Text = "See attendance";
            this.button12.UseVisualStyleBackColor = true;
            this.button12.Click += new System.EventHandler(this.button12_Click);
            // 
            // button11
            // 
            this.button11.Location = new System.Drawing.Point(358, 645);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(104, 37);
            this.button11.TabIndex = 35;
            this.button11.Text = "Mark present";
            this.button11.UseVisualStyleBackColor = true;
            this.button11.Click += new System.EventHandler(this.button11_Click);
            // 
            // button10
            // 
            this.button10.Location = new System.Drawing.Point(358, 688);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(104, 37);
            this.button10.TabIndex = 36;
            this.button10.Text = "Delete Attendance";
            this.button10.UseVisualStyleBackColor = true;
            this.button10.Click += new System.EventHandler(this.button10_Click);
            // 
            // dataGridView5
            // 
            this.dataGridView5.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView5.Location = new System.Drawing.Point(612, 603);
            this.dataGridView5.Name = "dataGridView5";
            this.dataGridView5.Size = new System.Drawing.Size(291, 182);
            this.dataGridView5.TabIndex = 51;
            this.dataGridView5.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView5_CellContentClick2);
            // 
            // button18
            // 
            this.button18.Location = new System.Drawing.Point(929, 602);
            this.button18.Name = "button18";
            this.button18.Size = new System.Drawing.Size(104, 37);
            this.button18.TabIndex = 52;
            this.button18.Text = "See submissions";
            this.button18.UseVisualStyleBackColor = true;
            this.button18.Click += new System.EventHandler(this.button18_Click);
            // 
            // button17
            // 
            this.button17.Location = new System.Drawing.Point(929, 645);
            this.button17.Name = "button17";
            this.button17.Size = new System.Drawing.Size(104, 37);
            this.button17.TabIndex = 53;
            this.button17.Text = "Grade";
            this.button17.UseVisualStyleBackColor = true;
            this.button17.Click += new System.EventHandler(this.button17_Click);
            // 
            // subId
            // 
            this.subId.Location = new System.Drawing.Point(1051, 662);
            this.subId.Name = "subId";
            this.subId.Size = new System.Drawing.Size(49, 20);
            this.subId.TabIndex = 54;
            // 
            // TeacherView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.AutoScrollMargin = new System.Drawing.Size(10, 10);
            this.ClientSize = new System.Drawing.Size(1164, 1016);
            this.Controls.Add(this.subId);
            this.Controls.Add(this.button17);
            this.Controls.Add(this.button18);
            this.Controls.Add(this.dataGridView5);
            this.Controls.Add(this.assiDate);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.labe20);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.assiDescription);
            this.Controls.Add(this.assiName);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.button9);
            this.Controls.Add(this.button13);
            this.Controls.Add(this.button14);
            this.Controls.Add(this.button15);
            this.Controls.Add(this.button10);
            this.Controls.Add(this.button11);
            this.Controls.Add(this.button12);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.labDate);
            this.Controls.Add(this.description);
            this.Controls.Add(this.curricula);
            this.Controls.Add(this.labNr);
            this.Controls.Add(this.button8);
            this.Controls.Add(this.button7);
            this.Controls.Add(this.button6);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.hobby);
            this.Controls.Add(this.grupa);
            this.Controls.Add(this.name);
            this.Controls.Add(this.email);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.emailBox);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.dataGridView4);
            this.Controls.Add(this.dataGridView3);
            this.Controls.Add(this.dataGridView2);
            this.Controls.Add(this.dataGridView1);
            this.Name = "TeacherView";
            this.Text = "TeacherView";
            this.Load += new System.EventHandler(this.TeacherView_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView5)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.DataGridView dataGridView4;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TextBox emailBox;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.TextBox email;
        private System.Windows.Forms.TextBox name;
        private System.Windows.Forms.TextBox grupa;
        private System.Windows.Forms.TextBox hobby;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.TextBox labNr;
        private System.Windows.Forms.TextBox curricula;
        private System.Windows.Forms.TextBox description;
        private System.Windows.Forms.DateTimePicker labDate;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button button13;
        private System.Windows.Forms.Button button14;
        private System.Windows.Forms.Button button15;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox assiName;
        private System.Windows.Forms.TextBox assiDescription;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label labe20;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.DateTimePicker assiDate;
        private System.Windows.Forms.DataGridView dataGridView3;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.DataGridView dataGridView5;
        private System.Windows.Forms.Button button18;
        private System.Windows.Forms.Button button17;
        private System.Windows.Forms.TextBox subId;
    }
}