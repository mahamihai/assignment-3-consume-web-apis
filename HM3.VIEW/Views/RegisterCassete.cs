﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using HM2.WEB.Models;
using Newtonsoft.Json;
using RestSharp;

namespace HM3.VIEW.Views
{
    public partial class RegisterCassete : Form
    {

        private RestClient client;
        public RegisterCassete(RestClient client)
        {
            InitializeComponent();
            this.client = client;
        }
        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            UserInfoApi newUser = new UserInfoApi()
            {
                token = this.textBox1.Text,
                email = this.textBox2.Text,
                name = this.textBox3.Text,
                grupa = this.textBox4.Text,
                hobby = this.textBox5.Text,
                username = this.textBox6.Text,
                password = this.textBox7.Text

            };
            var request = new RestRequest("User/register", Method.PUT);
            var json = JsonConvert.SerializeObject(newUser);
            request.AddParameter("application/json;charset=utf-8", json, ParameterType.RequestBody);
            MessageBox.Show(this.client.Execute(request).StatusCode.ToString());
        }
    }
}
