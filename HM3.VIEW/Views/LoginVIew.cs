﻿using System;
using System.Windows.Forms;
using RestSharp;
using RestSharp.Authenticators;

namespace HM3.VIEW.Views
{
    public partial class LoginVIew : Form
    {
        private RestClient client= new RestClient("http://localhost:53042");


        public LoginVIew()
        {
            InitializeComponent();


        }
        
        private async void button1_ClickAsync(object sender, EventArgs e)
        {
            string username = this.textBox1.Text.Trim();
            string password = this.textBox2.Text.Trim();


            var request = new RestRequest("User/login", Method.POST);

             request.AddParameter("username", username, ParameterType.QueryString);
             request.AddParameter("password", password, ParameterType.QueryString);

            var response = client.Execute(request).Content.ToString();
          UserModel received = Newtonsoft.Json.JsonConvert.DeserializeObject<UserModel>(response);
            if (received != null)
            {
                MessageBox.Show("Success");
                client.Authenticator = new HttpBasicAuthenticator(username, password);

                if (received.permission.Equals("student"))
                {
                    new StudentView(received,this.client).Show();
                }

                if (received.permission.Equals("teacher"))

                {
                    new TeacherView(this.client).Show();

                }
            }
            this.client= new RestClient("http://localhost:53042");
            


        }

        private void button2_Click(object sender, EventArgs e)
        {
            new RegisterCassete(this.client).Show();
        }
    }
}
