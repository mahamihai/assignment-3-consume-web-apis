﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using HM2.BLL.Models;
using HM2.WEB.Models;
using HM3.VIEW.Services;
using RestSharp;

namespace HM3.VIEW.Views
{
    public partial class TeacherView : Form
    {
    

        public TeacherView()
        {
            InitializeComponent();
        }

        private TeacherService _tService;
        public TeacherView(RestClient client)
        {
            InitializeComponent();
            this._tService=new TeacherService(client);
            this.AutoScroll = true;


        }

        private int studentId;
        private void TeacherView_Load(object sender, EventArgs e)
        {

        }
        private void dataGridView1_CellContentClick2(object sender, DataGridViewCellEventArgs e)
        {
            int id;

           
            try
            {
                int row = e.RowIndex;
                string value = this.dataGridView1.Rows[row].Cells["id"].Value.ToString();
                id = int.Parse(value);
            }
            catch (Exception ex)
            {
                string t = ex.ToString();
                return;
            }

            this.studentId = id;

        }

        private int laboratoryId;
        private void dataGridView2_CellContentClick2(object sender, DataGridViewCellEventArgs e)
        {
            int id;


            try
            {
                int row = e.RowIndex;
                string value = this.dataGridView2.Rows[row].Cells["id"].Value.ToString();
                id = int.Parse(value);
            }
            catch (Exception ex)
            {
                string t = ex.ToString();
                return;
            }

            this.laboratoryId = id;

        }

        private int attendanceId;
        private void dataGridView3_CellContentClick2(object sender, DataGridViewCellEventArgs e)
        {
            int id;


            try
            {
                int row = e.RowIndex;
                string value = this.dataGridView3.Rows[row].Cells["id"].Value.ToString();
                id = int.Parse(value);
            }
            catch (Exception ex)
            {
                string t = ex.ToString();
                return;
            }

            this.attendanceId = id;

        }

        private int assignmentId;
        private void dataGridView4_CellContentClick2(object sender, DataGridViewCellEventArgs e)
        {
            int id;


            try
            {
                int row = e.RowIndex;
                string value = this.dataGridView4.Rows[row].Cells["id"].Value.ToString();
                id = int.Parse(value);
            }
            catch (Exception ex)
            {
                string t = ex.ToString();
                return;
            }

            this.assignmentId = id;

        }

        private int submissionId;
        private void dataGridView5_CellContentClick2(object sender, DataGridViewCellEventArgs e)
        {
            int id;


            try
            {
                int row = e.RowIndex;
                string value = this.dataGridView5.Rows[row].Cells["id"].Value.ToString();
                id = int.Parse(value);
            }
            catch (Exception ex)
            {
                string t = ex.ToString();
                return;
            }

            this.submissionId = id;

        }

        private void button1_Click(object sender, EventArgs e)
        {
            var students = this._tService.getStudents();
            this.dataGridView1.DataSource = students;

        }

        private void button2_Click(object sender, EventArgs e)
        {
            this._tService.createStudent(this.emailBox.Text);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (this.studentId >0)
            {
                this._tService.DeleteStudent(this.studentId);
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            int selectedRow = this.dataGridView1.CurrentCell.RowIndex;
            StudentModel updateStudent = new StudentModel
            {
                id=this.studentId,
                token = this.dataGridView1.Rows[selectedRow].Cells["token"].Value.ToString(),
                email = this.email.Text,
                name = this.name.Text,
                grupa = this.grupa.Text,
                hobby = this.hobby.Text
            };
            MessageBox.Show(this._tService.UpdateStudent(updateStudent));

        }

        private void button5_Click(object sender, EventArgs e)
        {
            var laboratories = this._tService.getLabs();
            this.dataGridView2.DataSource = laboratories;
        }

        private void button6_Click(object sender, EventArgs e)
        {
            LaboratoryAPI newLab = new LaboratoryAPI
            {
                number = Int16.Parse(this.labNr.Text),
                date = this.labDate.Value,
                curricula = this.curricula.Text,
                description = this.description.Text
            };
            this._tService.createLab(newLab);

        }

        private void button7_Click(object sender, EventArgs e)
        {
            this._tService.deleteLab(this.laboratoryId);
        }

        private void button8_Click(object sender, EventArgs e)
        {
            LaboratoryModel newLab = new LaboratoryModel
            {
                id = this.laboratoryId,
                number = Int16.Parse(this.labNr.Text),
                date = this.labDate.Value,
                curricula = this.curricula.Text,
                description = this.description.Text
            };
            this._tService.updateLab(newLab);
        }

        private void button12_Click(object sender, EventArgs e)
        {
            var att = this._tService.getAtenndanceForLab(this.laboratoryId);
            this.dataGridView3.DataSource = att;
        }

        private void button11_Click(object sender, EventArgs e)
        {
            AttendanceAPI newAtt = new AttendanceAPI
            {
                labId = this.laboratoryId,
                studentId = this.studentId
            };
            this._tService.MarkPresent(newAtt);
        }

        private void button10_Click(object sender, EventArgs e)
        {
            this._tService.DeleteAttendance(this.attendanceId);
        }

        private void button15_Click(object sender, EventArgs e)
        {
            var labAssi = this._tService.getAssignmentsForLab(this.laboratoryId);
            this.dataGridView4.DataSource = labAssi;
        }

        private void button14_Click(object sender, EventArgs e)
        {
            AssignmentModel newAssi = new AssignmentModel
            {
                
                deadline = assiDate.Value,
                name = assiName.Text,
                description = assiDescription.Text,
                labId = this.laboratoryId
            };
            var t = this._tService.CreateAssignment(newAssi);
        }

        private void button13_Click(object sender, EventArgs e)
        {
            var t = this._tService.DeleteAssignment(this.assignmentId);
        }

        private void button18_Click(object sender, EventArgs e)
        {
            var submissions = this._tService.getAssignmentSubmissions(this.assignmentId);
            this.dataGridView5.DataSource = submissions;
        }

        private void button17_Click(object sender, EventArgs e)
        {
            int grade = Int32.Parse(this.subId.Text);
            this._tService.gradeSub(this.submissionId, grade,this.assignmentId);

        }
    }
}
