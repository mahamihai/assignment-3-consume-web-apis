﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using HM2.WEB.Models;
using HM3.VIEW.Services;
using RestSharp;

namespace HM3.VIEW.Views
{
    public partial class StudentView : Form
    {
        private RestClient client;
        private StudentService _sService;
        private UserModel _user;
        private StudentView()
        {
            InitializeComponent();
        }
        public StudentView(UserModel user,RestClient client)
        {
            InitializeComponent();
           
            this._sService=new StudentService(client);
            this._user = user;

        }

        private int laboratoryId;
        private void gridView_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int id;


            try
            {
                int row = e.RowIndex;
                string value = this.dataGridView1.Rows[row].Cells["id"].Value.ToString();
                id = int.Parse(value);
            }
            catch (Exception ex)
            {
                string t = ex.ToString();
                return;
            }

            this.laboratoryId = id;
        }

        private int assignmentId;
        private void gridView_CellClick1(object sender, DataGridViewCellEventArgs e)
        {
            int id;


            try
            {
                int row = e.RowIndex;
                string value = this.dataGridView2.Rows[row].Cells["id"].Value.ToString();
                id = int.Parse(value);
            }
            catch (Exception ex)
            {
                string t = ex.ToString();
                return;
            }

            this.assignmentId = id;
        }




        private void button1_Click(object sender, EventArgs e)
        {
            var labs = this._sService.getLabs();
            this.dataGridView1.DataSource = labs;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            var labs = this._sService.getLabsFiltered(this.filterBox.Text);
            this.dataGridView1.DataSource = labs;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            var assignments = this._sService.getLabAssignments(this.laboratoryId);
            this.dataGridView2.DataSource = assignments;

        }

        private void button4_Click(object sender, EventArgs e)
        {
            SubmissionAPI newSubmission = new SubmissionAPI
            {
                assignmentId = this.assignmentId,
                git = this.gitBox.Text,
                shortRemark = this.remarkBox.Text,
                studentId = this._user.id
            };
           MessageBox.Show( this._sService.submit(newSubmission));


        }
    }
}
