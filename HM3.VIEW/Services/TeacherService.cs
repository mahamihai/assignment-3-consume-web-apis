﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using HM2.BLL.Models;
using HM2.WEB.Models;
using HM3.VIEW.Modele;
using Newtonsoft.Json;
using RestSharp;

namespace HM3.VIEW.Services
{
   public class TeacherService
   {
       private RestClient client;

        public TeacherService(RestClient client)
        {
            this.client = client;
        }

        public List<StudentModel> getStudents()
        {
           var request = new RestRequest("Student/", Method.GET);



          var response = client.Execute<List<StudentModel>>(request).Data;
            return response;

        }

       public void createStudent(string email)
       {
           var request = new RestRequest("Student/", Method.POST);
           var json = JsonConvert.SerializeObject(email);


            request.AddParameter("application/json; charset=utf-8", json, ParameterType.RequestBody);

           
           client.Execute(request);


          
        }
       public void DeleteStudent(object id)
       {
           var request = new RestRequest("Student/{id}", Method.DELETE);
           var json = JsonConvert.SerializeObject(id);


           //request.AddParameter("application/json; charset=utf-8", json, ParameterType.RequestBody);
           request.AddParameter("id", id.ToString());

           client.Execute(request);



       }

       public string UpdateStudent(StudentModel upStudent)
       {
            var request=new RestRequest("Student/",Method.PUT);
           var json = JsonConvert.SerializeObject(upStudent);
           request.AddParameter("application/json; charset=utf-8", json, ParameterType.RequestBody);
            var answer=client.Execute(request).StatusCode;
           return answer.ToString();
       }

       public List<LaboratoryModel> getLabs()
       {
           var request=new RestRequest("Laboratory/",Method.GET);
           var response = client.Execute<List<LaboratoryModel>>(request).Data;
           return response;
       }

       public string createLab(LaboratoryAPI newLab)
       {
           var request = new RestRequest("Laboratory/", Method.POST);
           var json = JsonConvert.SerializeObject(newLab);
           request.AddParameter("application/json; charset=utf-8", json, ParameterType.RequestBody);
           var answer = client.Execute(request).StatusCode;
           return answer.ToString();
        }

       public string deleteLab(object id)
       {
           var request=new RestRequest("Laboratory/",Method.DELETE);
           var json = JsonConvert.SerializeObject(id);
           //request.AddParameter("application/json; charset=utf-8", json, ParameterType.RequestBody);
           request.AddParameter("id", id);
            var answer = client.Execute(request).StatusCode;
           return answer.ToString();
        }
       public string updateLab(LaboratoryModel lab)
       {
           var request = new RestRequest("Laboratory/", Method.PUT);
           var json = JsonConvert.SerializeObject(lab);
           request.AddParameter("application/json; charset=utf-8", json, ParameterType.RequestBody);

           var answer = client.Execute(request).StatusCode;
           return answer.ToString();
       }

       public List<AttendanceModel> getAtenndance()
       {
           var request = new RestRequest("Attendance/", Method.GET);
          

           var answer = client.Execute<List<AttendanceModel>>(request).Data;
           return answer;

        }
       public List<ViewAttendance> getAtenndanceForLab(object labId)
       {
           var allStudents = this.getStudents();
           var allAtt = this.getAtenndance();
           var labAtt = allAtt.Where(x => x.labId.Equals(labId)).Select(x =>


           {
               string SName = allStudents.Where(y => y.id.Equals(x.studentId)).Select(y => y.name).First();
               return new ViewAttendance
               {
                   id = x.id,
                   name = SName
               };
           }).ToList();
           return labAtt;

       }

       public string MarkPresent(AttendanceAPI newAtt)
       {
           var request = new RestRequest("Attendance/", Method.POST);
           var json = JsonConvert.SerializeObject(newAtt);
           request.AddParameter("application/json; charset=utf-8", json, ParameterType.RequestBody);

           var answer = client.Execute(request).StatusCode;
           return answer.ToString();
        }

       public string DeleteAttendance(object id)
       {
           var request = new RestRequest("Attendance/{id}", Method.DELETE);
           request.AddParameter("id", id);
           var answer = client.Execute(request).StatusCode;
           return answer.ToString();

        }

       public List<AssignmentModel> getAssignments()
       {
           var request = new RestRequest("Assignment/", Method.GET);
     

           var answer = client.Execute<List<AssignmentModel>>(request).Data;
           return answer;
        }
       public List<AssignmentModel> getAssignmentsForLab(object labId)
       {
           var allAssign = this.getAssignments();
           var labAssi = allAssign.Where(x => x.labId.Equals(labId)).ToList();
           return labAssi;
       }
       public string CreateAssignment(AssignmentModel newAss)
       {
           var request = new RestRequest("Assignment/", Method.POST);
           var json = JsonConvert.SerializeObject(newAss);
           request.AddParameter("application/json; charset=utf-8", json, ParameterType.RequestBody);

           var answer = client.Execute(request).StatusCode;
           return answer.ToString();
       }

       public string DeleteAssignment(object id)
       {
           var request = new RestRequest("Assignment/{id}", Method.DELETE);
           request.AddParameter("id", id);
           var answer = client.Execute(request).StatusCode;
           return answer.ToString();
        }

       public List<SubmissionModel> getAssignmentSubmissions(object assiId)
       {
           var request =new  RestRequest("Submission/{assiId}", Method.GET);
           request.AddParameter("assiId", assiId);
           var answer = client.Execute<List<SubmissionModel>>(request).Data;
           return answer;

       }
       
       public string gradeSub(int subId,int grade,int assiId)
       {
           var allSubmissions = this.getAssignmentSubmissions(assiId);
           var toUpdate = allSubmissions.Where(x => x.id.Equals(subId)).First();
           toUpdate.grade = grade;
           var request = new RestRequest("Submission/", Method.PUT);
           var json = JsonConvert.SerializeObject(toUpdate);
           request.AddParameter("application/json;charset=utf-8", json, ParameterType.RequestBody);
            var t =client.Execute(request).StatusCode.ToString();
          

           return t;
       }
    }
}
