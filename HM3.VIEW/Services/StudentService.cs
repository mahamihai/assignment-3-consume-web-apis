﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HM2.BLL.Models;
using HM2.WEB.Models;
using Newtonsoft.Json;
using RestSharp;

namespace HM3.VIEW.Services
{
  public  class StudentService
  {
      private RestClient client;
        public StudentService(RestClient client)
        {
            this.client = client;

        }
        public List<LaboratoryModel> getLabs()
        {
            var request = new RestRequest("Laboratory/", Method.GET);
            var response = client.Execute<List<LaboratoryModel>>(request).Data;
            return response;
        }
      public List<LaboratoryModel> getLabsFiltered(string filter)
      {
          var request = new RestRequest("Laboratory/{id}", Method.GET);
          request.AddParameter("id", filter);
          var response = client.Execute<List<LaboratoryModel>>(request).Data;

          return response;
      }

      public List<AssignmentModel> getLabAssignments(object labid)
      {

          var request = new RestRequest("Assignment/{labid}", Method.GET);
          request.AddParameter("labid", labid);
          var response = client.Execute<List<AssignmentModel>>(request).Data;

          return response;
      }
      public string submit(SubmissionAPI newSubmission)
      {
          var request = new RestRequest("Submission/", Method.POST);
          var json = JsonConvert.SerializeObject(newSubmission);


          request.AddParameter("application/json; charset=utf-8", json, ParameterType.RequestBody);
          request.OnBeforeDeserialization = resp => { resp.ContentType = "application/json"; };

            return client.Execute(request).Content;



      }


    }
}
